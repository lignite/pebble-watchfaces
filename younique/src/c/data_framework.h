#pragma once

#define NSLog(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_INFO, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSDebug(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_DEBUG, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSWarn(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_WARNING, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSError(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_ERROR, __FILE_NAME__, __LINE__, fmt, ## args);

#define APP_NAME "Younique"

#define LOG_ALL true

#define SETTINGS_KEY 0
#define STORAGE_VERSION_KEY 100
#define USER_WAS_1X_KEY 101
#define AMOUNT_OF_SETTINGS_CALLBACKS 2

#ifdef PBL_ROUND
#define WINDOW_FRAME GRect(0, 0, 180, 180)
#else
#define WINDOW_FRAME GRect(0, 0, 144, 168)
#endif

typedef enum SettingsCallback {
	SETTINGS_CALLBACK_MAIN_WINDOW = 0,
    SETTINGS_CALLBACK_INFO_LAYER
} SettingsCallback;

typedef enum WeatherStatus {
	WEATHER_STATUS_CLEAR_DAY = 0,
	WEATHER_STATUS_CLEAR_NIGHT,
	WEATHER_STATUS_WINDY,
	WEATHER_STATUS_COLD, //wut
	WEATHER_STATUS_PARTIALLY_CLOUDY_DAY,
	WEATHER_STATUS_PARTIALLY_CLOUDY_NIGHT,
	WEATHER_STATUS_HAZE,
	WEATHER_STATUS_LIGHT_CLOUDS,
	WEATHER_STATUS_RAINY,
	WEATHER_STATUS_SNOWING,
	WEATHER_STATUS_HAIL,
	WEATHER_STATUS_CLOUDY,
	WEATHER_STATUS_STORM,
	WEATHER_STATUS_NOT_AVAILABLE_OR_ERROR
} WeatherStatus;

typedef struct Weather {
	int16_t temperature;
	WeatherStatus condition;
} Weather;

typedef enum {
    ShakeModeShowDateAndBattery,
    ShakeModeShowEverything,
    ShakeModeDisabled,
    ShakeModeHideAll
} ShakeMode;

typedef struct Settings {
	bool btdisalert;
	bool btrealert;

    bool night_mode;
    bool show_island_image;
    bool large_time_font;
    bool imperial_measurements;

    ShakeMode shake_mode;

    GColor background_colour;
    GColor module_colour;
    GColor battery_bar_colour;

    char dateFormat[1][20];

    uint32_t photo_timeout;

    Weather weather;
} Settings;

typedef void (*SettingsChangeCallback)(Settings settings);

void data_framework_setup();
void data_framework_finish();
void process_tuple(Tuple *t, DictionaryIterator *iter, void *context);
void data_framework_inbox_dropped(AppMessageResult reason, void *context);
void data_framework_inbox(DictionaryIterator *iter, void *context);
Settings data_framework_get_settings();
unsigned int data_framework_hex_string_to_uint(char const* hexstring);
void data_framework_register_settings_callback(SettingsChangeCallback callback, SettingsCallback callbackIdentity);

extern Settings data_framework_local_settings;
extern SettingsChangeCallback settings_callbacks[AMOUNT_OF_SETTINGS_CALLBACKS];
