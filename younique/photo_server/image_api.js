var fs = require("fs");
var mongojs = require("mongojs");


/*

Younique Image API (adapted for no Lignite server involvement)
Written by Edwin Finch
Aug-Sept 2016

We recommend running this on a non-root user using PM2.
http://pm2.keymetrics.io/

This script used to be broken up into two parts: one part for Android-related APIs and the other for the main photo-processing APIs. However, to simplify the process of running one's own Younique server, I decided to consolidate them into one.

The default port is 6969, but you can change it to anything you'd like.

Once running, the endpoints are as follows:

/younique/convert
Used for converting photos from their raw format into a format compatible for Pebble.

/younique/delete
Used for deleting photos from the server.

/younique/android/create_session
Used for creating a user session for the Android workaround webpage.

/younique/android/get_session
Used for getting a user's full session for the Android workaround webpage based off their session token.

/younique/android/upload
Used for uploading photos from the Android workaround webpage.

/younique/android/delete
Used for deleting photos that were uploaded from the Android workaround webpage.

You can of course change these endpoints to fit any format you'd like.

Enjoy!

 */



/*
CONFIGURATION REQUIRED
 */
var privateKey  = fs.readFileSync("everyones_privatekey.pem", "utf8"); //Your HTTPS private key.
var certificate = fs.readFileSync("everyones_certificate.pem", "utf8"); //Your HTTPS certificate.

var port = 6969; //The port you want to run the server on.

var database = mongojs("database_config_here", ["youniqueUsers"]); //Your database instance.
var PHOTO_FOLDER = "/var/www/html/younique/photos"; //Must be a folder that's visible from the web. Make sure write permissions are set properly.
/*
CONFIGURATION REQUIRED
 */



/*

MAIN IMAGE-PROCESSING RELATED CODE
(Android code is below this section)

 */


var credentials = {
    key: privateKey,
    cert: certificate
};
var https = require("https");
var express = require("express");
var app = express();
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(port);

var gm = require("gm").subClass({
    imageMagick: true
});
var imagemin = require("imagemin");
var imageminPngquant = require("imagemin-pngquant");

var FORCED_HEIGHT = 0;

app.use(function(req, res, next) {
    var origin = req.headers.origin ? req.headers.origin : req.headers.host;
    res.setHeader("Access-Control-Allow-Origin", origin);
    res.header("Access-Control-Allow-Methods", "GET, OPTIONS, POST");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
    res.header("Access-Control-Allow-Credentials", true);
    return next();
});

function generateTemporaryFileName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 20; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

function validHeader(header) {
    var type = "unknown";
    switch (header) {
        case "89504e47":
            type = "image/png";
            break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
            type = "image/jpeg";
            break;
        default:
            type = "unknown";
            break;
    }
    return (type !== "unknown");
}

app.post("/younique/convert", function(request, response) {
    var data = "";
    request.on("data", function(chunk) {
        data += chunk + "";
        //console.log("got chunk " + chunk + " and data " + data);
    }).on("end", function() {
        var jsonData;
        //console.log("got data " + data);
        try {
            jsonData = JSON.parse(data);
        } catch (error) {
            console.log("Error: " + error);
            response.writeHead(200);
            response.end(JSON.stringify({
                head: 400,
                error: "Bad request."
            }));
            return;
        }

        var temporaryName = generateTemporaryFileName();
        var isJPEG = jsonData.image.includes("data:image/jpeg;base64,");
        var isPNG = jsonData.image.includes("data:image/png;base64,");

        if (!isJPEG && !isPNG) {
            response.writeHead(400);
            response.write(JSON.stringify({
                error: "Gutless! Trying to inject a file that's not an image."
            }));
            response.end();
            return;
        }

        var photoLocation = PHOTO_FOLDER + "/" + temporaryName;
        fs.writeFile(photoLocation + (isJPEG ? ".jpeg" : ".png"), jsonData.image.replace(isJPEG ? /^data:image\/jpeg;base64,/ : /^data:image\/png;base64,/, ""), "base64", function(err) {
            if (err) {
                console.log("error");
                return console.log(err);
            }
            fs.readFile(photoLocation + (isJPEG ? ".jpeg" : ".png"), function read(err, data) {
                if (err) {
                    throw err;
                }
                var arr = (new Uint8Array(data)).subarray(0, 4);
                var header = "";
                for (var i = 0; i < arr.length; i++) {
                    header += arr[i].toString(16);
                }
                console.log("Header " + header);
                if (!validHeader(header)) {
                    response.writeHead(400);
                    response.write(JSON.stringify({
                        error: "Gutless, trying to inject a file that's not an image."
                    }));
                    response.end();
                    return;
                }

                var imageMagickImage = gm(photoLocation + (isJPEG ? ".jpeg" : ".png"));
                var SIZE_LIMIT = 850;

                imageMagickImage.size(function(err, value){
                    if(err || !value){
                        console.error("Error " + err + " and value " + value);
                        if(!value){
                            value = {
                                width: 500,
                                height: 500
                            };
                            console.error("Manually set value!");
                        }
                    }
                    else if(value.width > SIZE_LIMIT || value.height > SIZE_LIMIT){
                        imageMagickImage.resize(SIZE_LIMIT, SIZE_LIMIT);
                    }

                    if(jsonData.cropData !== undefined){
                        imageMagickImage.rotate("white", jsonData.cropData.rotation);
                    }

                    // note : value may be undefined
                    if(jsonData.options.pebbleCrop){
                        imageMagickImage.crop(jsonData.cropData.size.w, jsonData.cropData.size.h, jsonData.cropData.origin.x, jsonData.cropData.origin.y)
                        .resize(jsonData.pebbleInfo.frame.size.w, jsonData.pebbleInfo.frame.size.h, "!");
                    }

                    var applyBwFilter = !jsonData.pebbleInfo.hasColour;

                    if(jsonData.options.style){
                        switch(jsonData.options.style){
                            case "coloured":
                                break;
                            case "bw":
                                applyBwFilter = true;
                                break;
                            case "blur":
                                imageMagickImage.blur(value.width);
                                break;
                            case "sepia":
                                imageMagickImage.sepia();
                                break;
                            case "invert":
                                imageMagickImage.negative();
                                break;
                        }
                    }

                    if(!jsonData.options.pleaseDontDitherForTheLoveOfGod){
                        if(jsonData.pebbleInfo.hasColour){
                            imageMagickImage.fill("#FFFFFF00")
                                .opaque("none")
                                .dither(true).out("FloydSteinberg")
                                .out("-remap")
                                .out("pebble_colours_64.gif")
                                .out("-define")
                                .out("png:compression-level=9")
                                .out("-define")
                                .out("png:compression-strategy=0")
                                .out("-define")
                                .out("png:exclude-chunk=all");
                        }
                    }

                    if(applyBwFilter){
                         imageMagickImage.fill("#FFFFFF00")
                             .opaque("none")
                             .dither(true).out("FloydSteinberg")
                             .out("-remap").out("bw_colours.gif")
                             .out("-define").out("png:compression-level=9")
                             .out("-define").out("png:compression-strategy=0")
                             .out("-define").out("png:exclude-chunk=all");
                    }

                    if(jsonData.pebbleInfo.platform === "chalk" && jsonData.options.pebbleCrop){
                        console.log("Clipping.");

                       imageMagickImage
                       .out("-alpha").out("set")
                       .out("chalk_mask.png").out("-compose").out("DstIn").out("-composite")
                    }

                    imageMagickImage.write(photoLocation + ".png", function(err) {
                            if (err) {
                                console.error(err);
                                return;
                            }

                            if(isJPEG){
                                fs.unlink(photoLocation + ".jpeg", function (error) {
                                    if (error){
                                        console.error(error);
                                        return;
                                    }
                                    console.log("Deletion of JPEG image sucessful.");
                                });
                            }

                            if(jsonData.options.pebbleCrop){
                                var pebbleImageLeftHalf = gm(photoLocation + ".png");
                                var left_half_png = photoLocation + "-pleft" + ".png";
                                var right_half_png = photoLocation + "-pright" + ".png";
                                pebbleImageLeftHalf.crop(jsonData.pebbleInfo.frame.size.w/2, jsonData.pebbleInfo.frame.size.h-FORCED_HEIGHT, 0, 0);
                                pebbleImageLeftHalf
                                .out("-background").out("white")
                                .out("-alpha").out("remove")
                                pebbleImageLeftHalf.write(left_half_png, function(lherror) {
                                    if (lherror) {
                                        console.error(lherror);
                                        return;
                                    }

                                    var pebbleImageRightHalf = gm(photoLocation + ".png");
                                    pebbleImageRightHalf.crop(jsonData.pebbleInfo.frame.size.w/2, jsonData.pebbleInfo.frame.size.h-FORCED_HEIGHT, jsonData.pebbleInfo.frame.size.w/2, 0);
                                    pebbleImageRightHalf
                                    .out("-background").out("white")
                                    .out("-alpha").out("remove")
                                    pebbleImageRightHalf.write(right_half_png, function(rherror) {
                                        if (rherror) {
                                            console.error(rherror);
                                            return;
                                        }

                                        response.writeHead(200);
                                        response.write(JSON.stringify({
                                            imageURL: "https://www.lignite.me/photo_uploader/photos/" + temporaryName + ".png",
                                            imageID: temporaryName,
                                            wasJPEG: isJPEG,
                                            pebbleURLs: {
                                                leftHalf: "https://www.lignite.me/photo_uploader/photos/" + temporaryName + "-pleft" + ".png",
                                                rightHalf: "https://www.lignite.me/photo_uploader/photos/" + temporaryName + "-pright" + ".png",
                                            }
                                        }));
                                        response.end();
                                    });
                                });
                            }
                            else{
                                response.writeHead(200);
                                response.write(JSON.stringify({
                                    imageURL: "https://www.lignite.me/photo_uploader/photos/" + temporaryName + ".png",
                                    imageID: temporaryName,
                                    wasJPEG: isJPEG
                                }));
                                response.end();
                            }
                        });
                });
            });
        });
    });
});

app.post("/younique/delete", function(request, response) {
    var data = "";
    request.on("data", function(chunk) {
        data += chunk + "";
        //console.log("got chunk " + chunk + " and data " + data);
    }).on("end", function() {
        var jsonData;
        //console.log("got data " + data);
        try {
            jsonData = JSON.parse(data);
        } catch (error) {
            console.log("Error: " + error);
            response.writeHead(200);
            response.end(JSON.stringify({
                head: 400,
                error: "Bad request."
            }));
            return;
        }

        if(jsonData.photoSessionData !== undefined){
            database.youniqueUsers.find({
                "token":jsonData.photoSessionData.token
            }, function(findError, foundDocs){
                if(findError){
                    return console.error(findError);
                }

                if(foundDocs.length !== 1){
                    return;
                }

                console.log("Checking user with session token " + jsonData.photoSessionData.token + " for " + jsonData.photoSessionData.imageID);

                var user =  foundDocs[0];
                console.log(JSON.stringify(user));
                for(var i = 0; i < user.photos.length; i++){
                    var photo = user.photos[i];
                    console.log("id " + photo.id + " for " + i);
                    if(photo.id === jsonData.photoSessionData.imageID){
                        console.log("Splicing " + photo.id + " at index " + i);
                        user.photos.splice(i, 1);
                        break;
                    }
                }

                database.youniqueUsers.update({
                    "token":jsonData.photoSessionData.token
                }, user, function(updateError, updateDocs){
                    if(updateError){
                        return console.error(updateError);
                    }
                });
            });
        }

        var photoLocation = PHOTO_FOLDER + "/" + jsonData.imageID + ".png";
        fs.unlink(photoLocation, function (error) {
            if (error){
                console.log("Error: " + error);
                response.writeHead(200);
                response.end(JSON.stringify({
                    head: 400,
                    error: error
                }));
                console.error(error);
                return;
            }
            console.log("Deleted main image " + jsonData.imageID);
            if(jsonData.pebbleCrop){
                var leftPhotoLocation = PHOTO_FOLDER + "/" + jsonData.imageID + "-pleft.png";
                fs.unlink(leftPhotoLocation, function (lherror) {
                    if (lherror){
                        console.log("Error: " + lherror);
                        response.writeHead(200);
                        response.end(JSON.stringify({
                            head: 400,
                            error: lherror
                        }));
                        console.error(lherror);
                        return;
                    }
                    var rightPhotoLocation = PHOTO_FOLDER + "/" + jsonData.imageID + "-pright.png";
                    fs.unlink(rightPhotoLocation, function (rherror) {
                        if (rherror){
                            console.log("Error: " + rherror);
                            response.writeHead(200);
                            response.end(JSON.stringify({
                                head: 400,
                                error: rherror
                            }));
                            console.error(rherror);
                            return;
                        }
                        console.log("Deletion of PNG through API with half images sucessful.");
                        response.writeHead(200);
                        response.end(JSON.stringify({
                            status: "deleted"
                        }));
                    });
                });
            }
            else{
                console.log("Deletion of PNG through API sucessful.");
                response.writeHead(200);
                response.end(JSON.stringify({
                    status: "deleted"
                }));
            }
        });
    });
});


/*

END MAIN IMAGE-PROCESSING RELATED CODE

 */




/*

ANDROID RELATED CODE

 */


 function generateRandomString() {
     var text = "";
     var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

     for (var i = 0; i < 10; i++) {
         text += possible.charAt(Math.floor(Math.random() * possible.length));
     }

     return text;
 }

 function validHeader(header) {
     var type = "unknown";
     switch (header) {
         case "89504e47":
             type = "image/png";
             break;
         case "ffd8ffe0":
         case "ffd8ffe1":
         case "ffd8ffe2":
             type = "image/jpeg";
             break;
         default:
             type = "unknown";
             break;
     }
     return (type !== "unknown");
 }

 function closeResponse(response, stringToCloseWith){
     response.writeHead(200);
     response.write(stringToCloseWith);
     response.end();
 }

 app.post("/younique/android/upload", function(request, response) {
     var data = "";
     request.on("data", function(chunk) {
         data += chunk + "";
     }).on("end", function() {
         var jsonData;
         try {
             jsonData = JSON.parse(data);
         } catch (error) {
             console.log("Error: " + error);
             response.writeHead(200);
             response.end(JSON.stringify({
                 head: 400,
                 error: "Bad request."
             }));
             return;
         }

         //console.log("Got " + data);

         var temporaryName = generateRandomString();
         var isJPEG = jsonData.image.includes("data:image/jpeg;base64,");
         var isPNG = jsonData.image.includes("data:image/png;base64,");

         if (!isJPEG && !isPNG) {
             closeResponse(response, JSON.stringify({
                 error: "We only support PNG and JPEG/JPG files, sorry."
             }));
             return;
         }

         var photoLocation = PHOTO_FOLDER + "/" + temporaryName;
         fs.writeFile(photoLocation + (isJPEG ? ".jpeg" : ".png"), jsonData.image.replace(isJPEG ? /^data:image\/jpeg;base64,/ : /^data:image\/png;base64,/, ""), "base64", function(err) {
             if (err) {
                 console.log("error");
                 return console.log(err);
             }
             fs.readFile(photoLocation + (isJPEG ? ".jpeg" : ".png"), function read(err, data) {
                 if (err) {
                     throw err;
                 }
                 var arr = (new Uint8Array(data)).subarray(0, 4);
                 var header = "";
                 for (var i = 0; i < arr.length; i++) {
                     header += arr[i].toString(16);
                 }
                 console.log("Header " + header);
                 if (!validHeader(header)) {
                     closeResponse(response, JSON.stringify({
                         error: "We only support PNG and JPEG/JPG files, sorry."
                     }));
                     return;
                 }

                 var imageMagickImage = gm(photoLocation + (isJPEG ? ".jpeg" : ".png"));
                 var SIZE_LIMIT = 850;

                 imageMagickImage.size(function(err, value){
                     if(err || !value){
                         console.error("Error " + err + " and value " + value);
                         if(!value){
                             value = {
                                 width: 500,
                                 height: 500
                             };
                             console.error("Manually set value!");
                         }
                     }
                     else if(value.width > SIZE_LIMIT || value.height > SIZE_LIMIT){
                         imageMagickImage.resize(SIZE_LIMIT, SIZE_LIMIT);
                     }

                     imageMagickImage.write(photoLocation + ".png", function(writeError) {
                         if (writeError) {
                             console.error(writeError);
                             closeResponse(response, JSON.stringify({
                                 uploaded: false
                             }));
                             return;
                         }

                         if(isJPEG){
                             fs.unlink(photoLocation + ".jpeg", function (deleteError) {
                                 if (deleteError){
                                     console.error(deleteError);
                                 }
                                 console.log("Deletion of JPEG image sucessful.");
                             });
                         }

                         var search = {"token":jsonData.sessionToken};
                         console.log("Searching for " + JSON.stringify(search));
                         database.youniqueUsers.find(search, function(findError, findDocs){
                             if(findError){
                                 fs.unlink(photoLocation + ".png", function (deleteError) {
                                     if (deleteError){
                                         console.error(deleteError);
                                     }
                                     console.log("Deletion of image sucessful.");
                                 });

                                 closeResponse(response, JSON.stringify({
                                     error: "There was an error connecting to the image database. Please contact us if this issue persists."
                                 }));
                                 return;
                             }

                             //console.log("Got " + JSON.stringify(findDocs));

                             if(findDocs.length !== 1){
                                 fs.unlink(photoLocation + ".png", function (deleteError) {
                                     if (deleteError){
                                         console.error(deleteError);
                                     }
                                     console.log("Deletion of image sucessful.");
                                 });

                                 closeResponse(response, JSON.stringify({
                                     error: ((findDocs.length === 0) ? "Unfortunately it seems your photo session either expired or never existed.\n\nPlease go back to Younique settings and generate another photo session to try again." : "Somehow there are multiple people using this session which is not allowed. Please contact us if this continues.")
                                 }));
                                 return;
                             }

                             var photoUploadInfo = {
                                 uploadTime: Date.now(),
                                 expires: Date.now() + (121*1000),
                                 id: temporaryName
                             };

                             var userSession = findDocs[0];
                             userSession.expires = photoUploadInfo.expires;
                             userSession.photos.push(photoUploadInfo);

                             database.youniqueUsers.update({"_id":userSession._id}, userSession, function(updateError, updateDocs){
                                 if(updateError){
                                     fs.unlink(photoLocation + ".png", function (deleteError) {
                                         if (deleteError){
                                             console.error(deleteError);
                                         }
                                         console.log("Deletion of image sucessful.");
                                     });

                                     closeResponse(response, JSON.stringify({
                                         error: "There was an error connecting to the image database. Please contact us if this issue persists."
                                     }));
                                     return;
                                 }

                                 (function(id, sessionToken, userSessionToUpdate){
                                     setTimeout(function(){
                                         database.youniqueUsers.find({"token":sessionToken}, function(sessionFindError, sessionFindDocs){
                                             if(sessionFindError){
                                                 fs.unlink(photoLocation + ".png", function (deleteError) {
                                                     if (deleteError){
                                                         console.error(deleteError);
                                                     }
                                                     console.log("Forced deletion of image sucessful.");
                                                 });
                                                 return;
                                             }
                                             if(sessionFindDocs.length !== 1){
                                                 console.error("There are " + sessionFindDocs.length + " sessions for this token, which isn't allowed.");
                                                 return;
                                             }
                                             var now = Date.now();
                                             var session = sessionFindDocs[0];

                                             if(session.expires > now){
                                                 console.log("Expiry time changed, I guess.");
                                             }
                                             else{
                                                 for(var i = 0; i < session.photos.length; i++){
                                                     var id = session.photos[i].id;
                                                     console.log("Deleting " + id + " at " + Date.now() + "...");
                                                     fs.unlink(PHOTO_FOLDER + "/" + id + ".png", function (deleteError) {
                                                         if (deleteError){
                                                             console.error(deleteError);
                                                         }
                                                         console.log("Deletion of image sucessful.");
                                                     });
                                                 }
                                                 if(session.photos.length > 0){
                                                     database.youniqueUsers.remove({"token":sessionToken}, function(deleteError, deleteDocs){
                                                         if(deleteError){
                                                             console.error(deleteError + " at the spotttt");
                                                             return;
                                                         }
                                                     });
                                                 }
                                             }
                                         });
                                     }, photoUploadInfo.expires-photoUploadInfo.uploadTime);
                                 })(temporaryName, jsonData.sessionToken, userSession);

                                 closeResponse(response, JSON.stringify(photoUploadInfo));
                             });
                         });
                     });
                 });
             });
         });
     });
 });

 app.post("/younique/android/delete", function(request, response) {
     var data = "";
     request.on("data", function(chunk) {
         data += chunk + "";
     }).on("end", function() {
         var jsonData;
         try {
             jsonData = JSON.parse(data);
         } catch (error) {
             console.log("Error: " + error);
             closeResponse(response, JSON.stringify({
                 error: "Bad request."
             }));
             return;
         }
         if(!jsonData.sessionToken){
             closeResponse(response, JSON.stringify({
                 error: "Bad request, please make sure you have a session token."
             }));
             return;
         }

         database.youniqueUsers.find({"token":jsonData.sessionToken}, function(findError, findDocs){
             if(findError){
                 closeResponse(response, JSON.stringify({
                     error: "There was an error connecting to the user database. Please contact us if this issue persists."
                 }));
                 return;
             }

             if(findDocs.length !== 1){
                 closeResponse(response, JSON.stringify({
                     error: "User not found."
                 }));
                 return;
             }

             var userToUpdate = findDocs[0];

             //console.log("Got user " + JSON.stringify(userToUpdate));

             //If the user already has a photo session and the expiry is still valid, return that instead of creating a new one.
             if(userToUpdate && userToUpdate.expires !== undefined){
                 if(userToUpdate.expires < Date.now()){
                     console.log("Photo session expiry date is less than now, must be deleted");
                     closeResponse(response, JSON.stringify({
                         deleted: true
                     }));
                     return;
                 }
             }

             var photoExists = false;
             for(var i = 0; i < userToUpdate.photos.length; i++){
                 var photo = userToUpdate.photos[i];
                 if(photo.id === jsonData.id){
                     photoExists = true;

                     (function(idToDelete){
                         fs.unlink(PHOTO_FOLDER + "/" + idToDelete + ".png", function (deleteError) {
                             if (deleteError){
                                 console.error(deleteError);
                             }
                             console.log("Deletion of image sucessful.");
                         });
                     })(photo.id);

                     userToUpdate.photos.splice(i, 1);

                     closeResponse(response, JSON.stringify({
                         deleted: true
                     }));
                     break;
                 }
             }
             if(!photoExists){
                 closeResponse(response, JSON.stringify({
                     deleted: true
                 }));
                 return;
             }

             database.youniqueUsers.update({"_id": userToUpdate._id}, userToUpdate, function(insertToSessionError, insertToSessionDocs){
                 if(insertToSessionError){
                     console.error(insertToSessionError);
                     return;
                 }
                 console.log("User profile updated.");
             });
         });
     });
 });

 app.post("/younique/android/create_session/", function(request, response) {
     var data = "";
     request.on("data", function(chunk) {
         data += chunk + "";
     }).on("end", function() {
         var jsonData;
         try {
             jsonData = JSON.parse(data);
         } catch (error) {
             console.log("Error: " + error);
             closeResponse(response, JSON.stringify({
                 error: "Bad request."
             }));
             return;
         }

         database.youniqueUsers.find({ "token":jsonData.sessionToken }, function(findError, findDocs){
             if(findError){
                 closeResponse(response, JSON.stringify({
                     error: "There was an error connecting to the user database. Please contact us if this issue persists."
                 }));
                 return;
             }

             var userToUpdate = findDocs[0];

             console.log("Got user " + JSON.stringify(userToUpdate));

             //If the user already has a photo session and the expiry is still valid, return that instead of creating a new one.
             if(userToUpdate){
                 if((userToUpdate.expires > Date.now()) || (userToUpdate.expires === undefined)){
                     closeResponse(response, JSON.stringify(userToUpdate));
                     return;
                 }
             }

             var token = generateRandomString();

             var newUser = {
                 token: token,
                 photos: []
             };
             database.youniqueUsers.insert(newUser, function(insertToSessionError, insertToSessionDocs){
                 if(insertToSessionError){
                     closeResponse(response, JSON.stringify({
                         error: "There was an error connecting to the session database. Please contact us if this issue persists. Error " + insertToSessionError + "."
                     }));
                     return;
                 }

                 console.log("Generated and created user " + JSON.stringify(newUser, undefined, 4));

                 closeResponse(response, JSON.stringify(newUser));
             });
         });
     });
 });

 app.post("/younique/android/get_session/", function(request, response) {
     var data = "";
     request.on("data", function(chunk) {
         data += chunk + "";
     }).on("end", function() {
         var jsonData;
         try {
             jsonData = JSON.parse(data);
         } catch (error) {
             console.log("Error: " + error);
             closeResponse(response, JSON.stringify({
                 error: "Bad request."
             }));
             return;
         }

         database.youniqueUsers.find({ "token":jsonData.sessionToken }, function(findError, findDocs){
             if(findError){
                 closeResponse(response, JSON.stringify({
                     error: "There was an error connecting to the user database. Please contact us if this issue persists."
                 }));
                 return;
             }

             var session = findDocs[0];

             console.log("Got session " + JSON.stringify(session));

             //If the user already has a photo session and the expiry is still valid, return that instead of creating a new one.
             if(session){
                 if((session.expires > Date.now()) || (session.expires === undefined)){
                     closeResponse(response, JSON.stringify(session));
                     return;
                 }
                 else{
                     closeResponse(response, JSON.stringify({
                         error: "Sorry, your previous photo session has expired to protect your photos. Please create a new photo session and speed it up a little bit."
                     }));
                 }
             }
             else{
                 closeResponse(response, JSON.stringify({
                     error: "No user was found with that token, sorry. Try creating a new session and starting again."
                 }));
             }
         });
     });
 });


/*

END ANDROID RELATED CODE

 */


console.log("Running Younique photo API on port " + port);