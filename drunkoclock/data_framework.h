#pragma once
#define CHAR_LIMIT 16
#define CURRENT_VERSION 11

typedef struct Settings {
	char customslottext[1][23];
	int8_t previousTemp;
	int8_t previousTempRaw; //Don't see how it'd be below 0 but you never know with climate change and all
	uint8_t updateFreq;
	uint8_t shakeAction;
	uint8_t wordPack;
	uint8_t slotUse;
	uint8_t previousCondition;
	uint8_t dateFormat;
	uint8_t animation;
	bool batteryBar;
	bool seconds;
	bool invert;
	bool btDisAlert;
	bool btReAlert;
	bool fahrenheit;
	bool progressiveDrunk;
	bool alerts;
} Settings;

//Yes I could save it as a persistent int but that doesn't work as well so we're doing this.
typedef struct DOCVersion {
	int16_t versionCode;
} DOCVersion;

/*
OUTDATED PERSISTENT STORAGE FORMAT
This old format was used for Drunk O' Clock 1.x. Users
are automatically updated to the new version on load.
*/

typedef struct persist{
	uint8_t theme;
	uint8_t wordPack;
	uint8_t slotUse;
	uint8_t extraScreen;
	uint8_t btDisAlert;
	uint8_t btReAlert;
	uint8_t updateInterval;
	uint8_t weatherInterval;
	uint8_t secondsEnabled;
	uint8_t batteryBar;
	uint8_t previousIcon;
	int8_t previousTemp;
	bool vibeOnUpdate;
	uint8_t tempPref;
	uint8_t updateHowOften;
	bool autoClose;
	bool customDate;
	uint8_t dateFormat;
	bool animations;
	char customslottext[1][23];
	bool progressiveDrunk;
	uint8_t language;
}__attribute__((__packed__)) persist;

typedef struct wordpack {
	char pack[8][30];
}__attribute__((__packed__)) wordpack;

/*
End outdated persistent storage format
*/

//i leik enum

typedef enum {
	SLOT_USE_DATE = 0,
	SLOT_USE_BLUETOOTH_STATUS,
	SLOT_USE_CUSTOM_TEXT,
	SLOT_USE_TEMPERATURE,
	SLOT_USE_NOTHING
}SlotUse;

typedef enum {
	SHAKE_USE_LAUNCH_EXTRA_SCREEN = 0,
	SHAKE_USE_CHANGE_SWEAR_WORD,
	SHAKE_USE_SIMPLICITY,
	SHAKE_USE_DISABLED
}ShakeUse;

typedef enum {
	WORDPACK_SOBER = 0,
	WORDPACK_TIPSY,
	WORDPACK_WASTED
}WordPackUse;

typedef enum {
	ANIMATION_CLASSIC = 0,
	ANIMATION_VERTICLE,
	ANIMATION_WACKY,
	ANIMATION_SHAKEUP,
	ANIMATION_DISABLED
}AnimationUse;

typedef enum {
	DATE_FORMAT_DDMMYY = 0,
	DATE_FORMAT_MMDDYY,
	DATE_FORMAT_MONTHDAYYEAR,
	DATE_FORMAT_WEEKDAY
}DateFormatUse;

typedef enum {
	KEY_SETTINGS = 0,
	KEY_WORDPACK_SOBER,
	KEY_WORDPACK_TIPSY,
	KEY_WORDPACK_WASTED,
	KEY_SLOTPACK_SOBER,
	KEY_SLOTPACK_TIPSY,
	KEY_SLOTPACK_WASTED,
	KEY_BTPACK_SOBER,
	KEY_BTPACK_TIPSY,
	KEY_BTPACK_WASTED,
	KEY_WPACK_SOBER,
	KEY_WPACK_TIPSY,
	KEY_WPACK_WASTED,
	KEY_VERSION
}PersistentKey;

int get_random_int(bool specific, int specificTop);
char *get_main_swear_word(bool altBuffer, bool generateNew);
int load_settings();
int save_settings();
void send_weather_request();
Settings get_settings();
void write_settings(Settings newSettings);
void inbox(DictionaryIterator *iter, void *context);
