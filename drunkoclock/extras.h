#pragma once

void create_fonts();
TextLayer* text_layer_init(GRect location, GTextAlignment alignment, int font);
void animate_layer(Layer *layer, GRect *start, GRect *finish, int duration, int delay);
void notify_bar_push_notif(char *sentence, int vibrateNum);
void notify_bar_create(Window *w);
void notify_bar_set_enabled(bool enabled);
void adapt_font_size_to_frame(TextLayer *layer);
GRect get_new_frame(TextLayer* layer, char *new_word);
void fire_animation(TextLayer* text_layer, int offset, GRect nextFrame, bool swear_word);
void set_animation_use(int use);
void update_font_callback(void *data);