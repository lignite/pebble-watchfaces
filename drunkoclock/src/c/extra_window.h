#pragma once
#define EXTRA_WINDOW_STAY_OPEN_LENGTH 15

#ifdef PBL_ROUND
#define WINDOW_FRAME GRect(0, 0, 180, 180)
#else
#define WINDOW_FRAME GRect(0, 0, 144, 168)
#endif

void extra_window_update_settings_raw(Settings settings);
void extra_window_update_settings(Settings settings, TickHandler tick_handler);
void extra_window_update_weather();
void extra_window_update_bluetooth(bool connected);
void extra_window_tick_handler(struct tm *t, TimeUnits unit);
void extra_window_prepare_for_launch();
void window_load_extra(Window *window);
void window_unload_extra(Window *window);
Window *extra_window_get_window();
void extra_window_create();
void extra_window_destroy();
bool extra_window_is_loaded();
