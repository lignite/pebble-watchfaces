#include <pebble.h>
#include "lignite.h"
#include "extra_window.h"
#include "phrases.h"

Layer *lines_e;
TextLayer *time_e, *weather1_e, *weather2_e, *date_e, *bluetooth_e;
Window *extra_window;
Settings extra_settings;
TickHandler previoushandler;
bool extra_window_loaded = false;
int open_for = -1;

void draw_lines(Layer *layer, GContext *ctx){
	graphics_context_set_fill_color(ctx, GColorWhite);
	GRect whiteFrameToFill = GRect(0, 0, WINDOW_FRAME.size.w, WINDOW_FRAME.size.h/2);
	graphics_fill_rect(ctx, whiteFrameToFill, 0, GCornerNone);

	graphics_context_set_fill_color(ctx, GColorBlack);
	GRect currentTimeFrame = layer_get_frame(text_layer_get_layer(time_e));
	int8_t amountOfPadding = 4;
	GRect timeFrameToFill = GRect(currentTimeFrame.origin.x - amountOfPadding, currentTimeFrame.origin.y, currentTimeFrame.size.w + (amountOfPadding*2), currentTimeFrame.size.h + (amountOfPadding*2));
	////NSLog("Current frame GRect(%d, %d, %d, %d)", timeFrameToFill.origin.x, timeFrameToFill.origin.y, timeFrameToFill.size.w, timeFrameToFill.size.h);
	graphics_fill_rect(ctx, timeFrameToFill, 5, GCornersAll);
}

//Quick fix
void extra_window_update_settings_raw(Settings settings){
	extra_settings = settings;
}

void extra_window_update_settings(Settings settings, TickHandler tick_handler){
	previoushandler = tick_handler;
	extra_settings = settings;
}

bool extra_window_is_loaded(){
	return extra_window_loaded;
}

void extra_window_update_info(){
	if(EXTRA_WINDOW_STAY_OPEN_LENGTH-open_for == 0){
		window_stack_pop(true);
		tick_timer_service_subscribe(MINUTE_UNIT, previoushandler);
	}
}

void extra_window_update_weather(){
	static char condition_buffer[] = "It's FUCKING SNOWING YOU CANADIAN";
	snprintf(condition_buffer, sizeof(condition_buffer), "%s", phrases_get_weather(extra_settings.currentWeather.icon));
	text_layer_set_text(weather1_e, condition_buffer);

	static char degrees_buffer[] = "and -1337 degrees.";
	snprintf(degrees_buffer, sizeof(degrees_buffer), "and %d degrees.", extra_settings.currentWeather.temperature);
	text_layer_set_text(weather2_e, degrees_buffer);
}

void extra_window_update_bluetooth(bool connected){
	static char bluetooth_buffer[] = "Fucking connected, bitch";
	snprintf(bluetooth_buffer, sizeof(bluetooth_buffer), "%s", phrases_get_bluetooth(connected));
	text_layer_set_text(bluetooth_e, bluetooth_buffer);
}

void extra_window_update_date(struct tm *t){
	static char day_buffer[] = "Wednesday";
	strftime(day_buffer, sizeof(day_buffer), "%A", t);

	static char date_buffer[] = "Blimey, it's Wednesday!!!";
	snprintf(date_buffer, sizeof(date_buffer), "%s, it's %s.", phrases_get_slot_swear(), day_buffer);
	text_layer_set_text(date_e, date_buffer);
}

void extra_window_tick_handler(struct tm *t, TimeUnits unit){
	if(unit == MINUTE_UNIT){
		static char time_buffer[] = "00:00";
		if(clock_is_24h_style()){
			strftime(time_buffer, sizeof(time_buffer), "%H:%M", t);
		}
		else{
			strftime(time_buffer, sizeof(time_buffer), "%I:%M", t);
		}
		text_layer_set_text(time_e, time_buffer);
		extra_window_update_date(t);
	}

	open_for++;
	extra_window_update_info();
}

void extra_window_prepare_for_launch(){
	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);

	extra_window_tick_handler(t, MINUTE_UNIT);

	tick_timer_service_subscribe(SECOND_UNIT, extra_window_tick_handler);

	extra_window_update_weather();
	extra_window_update_bluetooth(bluetooth_connection_service_peek());
	extra_window_loaded = true;
}

void window_load_extra(Window *window){
	//NSLog("Loading extra window...");
	Layer *window_layer = window_get_root_layer(window);

	lines_e = layer_create(GRect(0, 0, WINDOW_FRAME.size.w, WINDOW_FRAME.size.h));
	layer_set_update_proc(lines_e, draw_lines);
	layer_add_child(window_layer, lines_e);

	time_e = text_layer_init(GRect(PBL_IF_ROUND_ELSE(50, 32), PBL_IF_ROUND_ELSE(15, 10), 80, 40), GTextAlignmentCenter, fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_BEBAS_NEUE_38)));
	text_layer_set_text_color(time_e, GColorWhite);
	layer_add_child(window_layer, text_layer_get_layer(time_e));

	weather1_e = text_layer_init(GRect(3, PBL_IF_ROUND_ELSE(95, 90), WINDOW_FRAME.size.w, 50), GTextAlignmentCenter, fonts_get_system_font(FONT_KEY_GOTHIC_18));
	layer_add_child(window_layer, text_layer_get_layer(weather1_e));

	weather2_e = text_layer_init(GRect(3, PBL_IF_ROUND_ELSE(115, 110), WINDOW_FRAME.size.w, 50), GTextAlignmentCenter, fonts_get_system_font(FONT_KEY_GOTHIC_18));
	layer_add_child(window_layer, text_layer_get_layer(weather2_e));

	date_e = text_layer_init(GRect(3, PBL_IF_ROUND_ELSE(65, 60), WINDOW_FRAME.size.w, 30), GTextAlignmentCenter, fonts_get_system_font(FONT_KEY_GOTHIC_18));
	text_layer_set_text_color(date_e, GColorBlack);
	layer_add_child(window_layer, text_layer_get_layer(date_e));

	bluetooth_e = text_layer_init(GRect(3, PBL_IF_ROUND_ELSE(140, 135), WINDOW_FRAME.size.w, 60), GTextAlignmentCenter, fonts_get_system_font(FONT_KEY_GOTHIC_18));
	layer_add_child(window_layer, text_layer_get_layer(bluetooth_e));

	extra_window_prepare_for_launch();
	//NSLog("Extra window loaded.");
}

void window_unload_extra(Window *window){
	//NSLog("Unloading extra window...");

	tick_timer_service_unsubscribe();
	layer_destroy(lines_e);
	lines_e = NULL;
	text_layer_destroy(weather1_e);
	weather1_e = NULL;
	text_layer_destroy(weather2_e);
	weather2_e = NULL;
	text_layer_destroy(time_e);
	time_e = NULL;
	text_layer_destroy(date_e);
	date_e = NULL;
	text_layer_destroy(bluetooth_e);
	bluetooth_e = NULL;

	extra_window_loaded = false;
	open_for = -1;

	//NSLog("Extra window unloaded.");
}

Window *extra_window_get_window(){
	if(time_e != NULL){
		extra_window_prepare_for_launch();
	}
	return extra_window;
}

void extra_window_create(){
	extra_window = window_create();
	window_set_background_color(extra_window, GColorBlack);
	window_set_window_handlers(extra_window, (WindowHandlers){
		.load = window_load_extra,
		.unload = window_unload_extra,
	});
}

void extra_window_destroy(){
	window_destroy(extra_window);
}
