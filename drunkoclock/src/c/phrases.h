#pragma once

typedef struct WordPack {
	char pack[5][30];
} WordPack;

typedef struct WeatherPack {
	char pack[8][30];
} WeatherPack;

typedef struct BluetoothPack {
	char connected[3][24];
	char disconnected[3][24];
} BluetoothPack;

typedef struct SlotPack{
	char pack[6][8];
} SlotPack;

void phrases_set_settings(Settings settings);
int phrases_save();
int phrases_read();
void phrases_strcpy(int key, char *new_string);
char *phrases_get_main_swear(bool altBuffer, bool generateNew);
const char *phrases_get_slot_swear();
const char *phrases_get_bluetooth(bool connected);
const char *phrases_get_weather();
