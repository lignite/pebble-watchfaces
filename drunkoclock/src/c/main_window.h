#pragma once

#ifdef PBL_ROUND
#define CENTER GPoint(90, 90)
#define WINDOW_SIZE GSize(180, 180)
#define IS_ROUND true
#define WINDOW_FRAME GRect(0, 0, 180, 180)
#else
#define CENTER GPoint(72, 84)
#define WINDOW_SIZE GSize(144, 168)
#define IS_ROUND false
#define WINDOW_FRAME GRect(0, 0, 144, 168)
#endif

typedef struct {
    char word[2][10];
    bool alternate;
    TextLayer *layerToUpdate;
    GRect originalFrame;
} NewWord;

void main_window_init();
void main_window_deinit();
Window *main_window_get_window();
void main_window_update_settings(Settings settings);
void fire_animation_callback();
void update_word_callback();