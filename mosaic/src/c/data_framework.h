#pragma once

#define NSLog(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_INFO, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSWarn(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_WARNING, __FILE_NAME__, __LINE__, fmt, ## args);

#define NSError(fmt, args...)                                \
  app_log(APP_LOG_LEVEL_ERROR, __FILE_NAME__, __LINE__, fmt, ## args);

#define APP_NAME "Mosaic"

#define LOG_ALL false

#ifdef PBL_COLOR
#define PBL_COLOUR
#endif

#define SETTINGS_KEY 0
#define STORAGE_VERSION_KEY 100
#define USER_WAS_1X_KEY 101
#define AMOUNT_OF_SETTINGS_CALLBACKS 2

typedef enum AppKey {
    //Do not delete these
    APP_KEY_SECURITY_EMAIL = 1000,
    APP_KEY_SECURITY_ACCESS_CODE = 1001,
    APP_KEY_SECURITY_CHECKSUM = 1002,
    //Do not delete these ^

	APP_KEY_BTDISALERT = 0,
	APP_KEY_BTREALERT,
    APP_KEY_ANIMATION_TYPE,
	APP_KEY_SHAKE_TYPE,
    APP_KEY_BATTERY_INDICATOR,
    APP_KEY_NUMBER_COLOUR,
    APP_KEY_BACKGROUND_COLOUR,
    APP_KEY_OLD_STYLE
} AppKey;

typedef enum SettingsCallback {
	SETTINGS_CALLBACK_MAIN_WINDOW = 0,
	SETTINGS_CALLBACK_GRAPHICS
} SettingsCallback;

typedef enum {
    AnimationTypeNone,
    AnimationTypeDazzle,
    AnimationTypeSpiralIn,
    AnimationTypeSnakeFromTop
} AnimationType;

typedef enum {
    ShakeTypeNone,
    ShakeTypeAnimate,
    ShakeTypeShuffle,
    ShakeTypeDisplayNumbers
} ShakeType;

typedef struct v1Settings {
	bool btdisalert;
	bool btrealert;

    AnimationType animationType;
    ShakeType shakeType;
    bool batteryIndicator;
	GColor backgroundColour;
	GColor numbersColour;
} v1Settings;

typedef struct Settings {
	bool btdisalert;
	bool btrealert;

    AnimationType animationType;
    ShakeType shakeType;
    bool batteryIndicator;
	GColor backgroundColour;
	GColor numbersColour;

    bool oldStyle;
} Settings;

typedef void (*SettingsChangeCallback)(Settings settings);

void data_framework_setup();
void data_framework_finish();
void process_tuple(Tuple *t);
void data_framework_inbox_dropped(AppMessageResult reason, void *context);
void data_framework_inbox(DictionaryIterator *iter, void *context);
Settings data_framework_get_settings();
unsigned int data_framework_hex_string_to_uint(char const* hexstring);
const char* get_gcolor_text(GColor m_color);
void data_framework_register_settings_callback(SettingsChangeCallback callback, SettingsCallback callbackIdentity);

extern Settings data_framework_local_settings;
extern SettingsChangeCallback settings_callbacks[AMOUNT_OF_SETTINGS_CALLBACKS];
