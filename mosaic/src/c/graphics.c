#include <pebble.h>
#include <math.h>
#include "lignite.h"
#include "graphics.h"

void graphics_launch_animation_for_square(Square *square, int delay);
void graphics_launch_shuffle_animation_for_square(Square *squareToFirst, int delay);
void graphics_reset_all_squares();

Layer *graphics_layer, *rotate_cover_layer;
Square *squares[AMOUNT_OF_SQUARES_X][AMOUNT_OF_SQUARES_Y];
int minute, hour;
bool rotate_layer_created = false, displayNumbers = true, animating = false;
Settings graphics_settings;
BatteryChargeState graphics_charge_state;

const uint8_t grid_numbers[10][5][7] = {
    {
        { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 1, 0, 0, 0, 1, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 2, 2, 2, 2, 2, 2, 2 }, { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }, { 2, 2, 2, 2, 2, 2, 2 }
    },
    {
        { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 0, 1, 1, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0 }, { 0, 1, 1, 1, 0, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 0, 0, 2, 2 }, { 0, 1, 1, 1, 0, 2, 2 }, { 0, 0, 0, 1, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 2, 2, 2, 2 }, { 0, 1, 0, 2, 2, 2, 2 }, { 0, 1, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 1, 0, 1, 0, 1, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    },
    {
        { 0, 0, 0, 0, 0, 2, 2 }, { 0, 1, 1, 1, 0, 2, 2 }, { 0, 1, 0, 1, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0 }
    }
};

GColor graphics_get_time_colour_for_square(Square *square){
    int toGrab = 0;
    switch(square->timeRange){
        case TimeRangeTopLeft:
            toGrab = hour/10;
            break;
        case TimeRangeTopRight:
            toGrab = hour % 10;
            break;
        case TimeRangeBottomLeft:
            toGrab = minute/10;
            break;
        case TimeRangeBottomRight:
            toGrab = minute % 10;
            break;
        default:
            break;
    }
    int rawX = square->rawLocation.x, rawY = square->rawLocation.y;
    rawX -= SQUARE_PADDING_LEFT_RIGHT + square->timeRange /2;
    rawY -= SQUARE_PADDING_TOP_BOTTOM + (square->timeRange  > 2);
    if(square->timeRange == 2 || square->timeRange == 4){
        #ifdef PBL_ROUND
        rawX--;
        #endif
        if(grid_numbers[toGrab-1][rawX][rawY] == 1){
            #ifdef PBL_COLOR
                return graphics_settings.numbersColour;
            #else
                return GColorBlack;
            #endif
        }
        else if(grid_numbers[toGrab-1][rawX][rawY] == 2){
            return GColorFromRGB((rand() % 255), (rand() % 255), (rand() % 255));
        }
    }
    else{
        if(grid_numbers[toGrab][rawX][rawY] == 1){
            #ifdef PBL_COLOR
                return graphics_settings.numbersColour;
            #else
                return GColorBlack;
            #endif
        }
        else if(grid_numbers[toGrab][rawX][rawY] == 2){
            return GColorFromRGB((rand() % 255), (rand() % 255), (rand() % 255));
        }
    }
    return graphics_settings.backgroundColour;
}

uint8_t graphics_get_time_range_for_square(Square *square){
    int i = square->location.x;
    int i1 = square->location.y;
    GSize padding = GSize(SQUARE_PADDING_LEFT_RIGHT*HEIGHT_AND_WIDTH - 3, SQUARE_PADDING_TOP_BOTTOM*HEIGHT_AND_WIDTH - 1);

    GRect firstFrame = GRect(padding.w, padding.h, NUMBER_SIZE.w*HEIGHT_AND_WIDTH, NUMBER_SIZE.h*HEIGHT_AND_WIDTH);
    GRect secondFrame = GRect(WINDOW_SIZE.w-padding.w-firstFrame.size.w, firstFrame.origin.y, firstFrame.size.w, firstFrame.size.h);
    GRect thirdFrame = GRect(firstFrame.origin.x, WINDOW_SIZE.h-padding.h-firstFrame.size.h, firstFrame.size.w, firstFrame.size.h);
    GRect fourthFrame = GRect(secondFrame.origin.x, thirdFrame.origin.y, firstFrame.size.w, firstFrame.size.h);

    if((i >= firstFrame.origin.x && i1 >= firstFrame.origin.y) && (i < (firstFrame.size.w+firstFrame.origin.x) && i1 < (firstFrame.size.h+firstFrame.origin.y))){
        return TimeRangeTopLeft;
    }
    else if((i >= secondFrame.origin.x && i1 >= secondFrame.origin.y) && (i < (secondFrame.size.w+secondFrame.origin.x) && i1 < (secondFrame.size.h+secondFrame.origin.y))){
        return TimeRangeTopRight;
    }
    else if((i >= thirdFrame.origin.x && i1 >= thirdFrame.origin.y) && (i < (thirdFrame.size.w+thirdFrame.origin.x) && i1 < (thirdFrame.size.h+thirdFrame.origin.y))){
        return TimeRangeBottomLeft;
    }
    else if((i >= fourthFrame.origin.x && i1 >= fourthFrame.origin.y) && (i < (fourthFrame.size.w+fourthFrame.origin.x) && i1 < (fourthFrame.size.h+fourthFrame.origin.y))){
        return TimeRangeBottomRight;
    }
    return TimeRangeNone;
}

GColor graphics_get_colour_for_square(Square *square){
    if(square->timeRange != TimeRangeNone && displayNumbers == true){
        return graphics_get_time_colour_for_square(square);
    }
    else{
        #ifdef PBL_COLOR
            return GColorFromRGB((rand() % 255), (rand() % 255), (rand() % 255));
        #else
            return GColorBlack;
        #endif
    }
}

void graphics_set_time(struct tm *t){
    minute = t->tm_min;
    hour = t->tm_hour;
    if(hour > 12 && !clock_is_24h_style()){
        hour -= 12;
    }

    for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
        for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
            squares[i][i1]->colour = graphics_get_colour_for_square(squares[i][i1]);
        }
    }

    layer_mark_dirty(graphics_layer);
}

#ifndef PBL_ROUND
void graphics_draw_square_radial(GContext *ctx, int amountToDraw){
    graphics_context_set_stroke_width(ctx, 3);

    GPoint previousPoint = GPoint(72, 3);
    for(int i = 0; i < 5; i++){
        int16_t costOfLine = 0;
        GPoint position = GPoint(72, 3);
        bool vertical = false;
        switch(i){
            case 0:
                costOfLine = 69;
                break;
            case 1:
                position.x = 141;
                costOfLine = 162;
                vertical = true;
                break;
            case 2:
                position.x = 141;
                position.y = 165;
                costOfLine = -138;
                break;
            case 3:
                position.x = 3;
                position.y = 165;
                costOfLine = -162;
                vertical = true;
                break;
            case 4:
                position.x = 3;
                costOfLine = 69;
                break;
        }
        amountToDraw -= abs(costOfLine);
        if(amountToDraw < 0) costOfLine += (costOfLine < 0) ? abs(amountToDraw) : amountToDraw;

        GPoint nextPoint = !vertical ? GPoint(position.x+costOfLine, position.y) : GPoint(position.x, position.y+costOfLine);
        graphics_draw_line(ctx, previousPoint, nextPoint);
        previousPoint = nextPoint;

        if(amountToDraw < 0) break;
    }
}
#endif

void graphics_proc(Layer *layer, GContext *ctx){
    for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
        for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
            Square *s = squares[i][i1];
            graphics_context_set_fill_color(ctx, s->colour);
            if(s->display){
                graphics_fill_rect(ctx, GRect(s->location.x, s->location.y, s->heightAndWidth, s->heightAndWidth), 0, GCornerNone);
            }
        }
    }
}

void graphics_settings_callback(Settings new_settings){
    bool reloadAll = false;
    if(new_settings.oldStyle != graphics_settings.oldStyle){
        reloadAll = true;
    }
    graphics_settings = new_settings;

    if(reloadAll){
        for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
            for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
                square_destroy(squares[i][i1]);
                Square *newSquare = square_create(i, i1);
                newSquare->colour = graphics_get_colour_for_square(newSquare);
                newSquare->timeRange = graphics_get_time_range_for_square(newSquare);

                squares[i][i1] = newSquare;
            }
        }
    }

    graphics_reset_all_squares();

    layer_mark_dirty(graphics_layer);
}

typedef enum {
    DirectionRight,
    DirectionDown,
    DirectionLeft,
    DirectionUp
} Direction;

Direction currentDirection;
uint8_t currentRowFilled[4];

void graphics_square_animation(void *data){
    Square *square = (Square*)data;

    square->display = true;

    layer_mark_dirty(graphics_layer);

    Square *newSquareToLoad = NULL;

    switch(graphics_settings.animationType){
        case AnimationTypeDazzle:{
            int amountOfChecks = 0;
            int8_t CHECK_THRESHHOLD = 100;
            newSquareToLoad = squares[rand() % AMOUNT_OF_SQUARES_X][rand() % AMOUNT_OF_SQUARES_Y];
            while(newSquareToLoad->display){
                if(amountOfChecks > CHECK_THRESHHOLD){
                    bool breakLoop = true;
                    for(int xi = 0; xi < AMOUNT_OF_SQUARES_X; xi++){
                        for(int yi = 0; yi < AMOUNT_OF_SQUARES_Y; yi++){
                            if(!squares[xi][yi]->display){
                                breakLoop = false;
                                break;
                            }
                        }
                    }
                    if(breakLoop){
                        amountOfChecks = -1;
                        newSquareToLoad = NULL;
                        break;
                    }
                    else{
                        CHECK_THRESHHOLD = 200;
                    }
                }
                newSquareToLoad = squares[rand() % AMOUNT_OF_SQUARES_X][rand() % AMOUNT_OF_SQUARES_Y];
                amountOfChecks++;
            }

            break;
        }
        case AnimationTypeSpiralIn:{
            if(square->rawLocation.x == 0 && square->rawLocation.y == 0){
                //APP_LOG(APP_LOG_LEVEL_INFO, "Switching to right");
                currentDirection = DirectionRight;
                for(int i = 0; i < 4; i++){
                    currentRowFilled[i] = 0;
                }
            }

            GPoint currentPoint = square->rawLocation;
            GPoint newPoint = currentPoint;
            switch(currentDirection){
                case DirectionRight:
                    newPoint.x++;
                    if(newPoint.x >= AMOUNT_OF_SQUARES_X-currentRowFilled[DirectionRight]){
                        currentDirection = DirectionDown;
                        newPoint.x--;
                        currentRowFilled[DirectionRight]++;
                    }
                    else{
                        break;
                    }
                case DirectionDown:
                    newPoint.y++;
                    if(newPoint.y >= AMOUNT_OF_SQUARES_Y-currentRowFilled[DirectionDown]){
                        currentDirection = DirectionLeft;
                        newPoint.y--;
                        currentRowFilled[DirectionDown]++;
                    }
                    else{
                        break;
                    }
                case DirectionLeft:
                    newPoint.x--;
                    if(newPoint.x < currentRowFilled[DirectionLeft]){
                        currentDirection = DirectionUp;
                        newPoint.x++;
                        currentRowFilled[DirectionLeft]++;
                    }
                    else{
                        break;
                    }
                case DirectionUp:
                    newPoint.y--;
                    if(newPoint.y < (currentRowFilled[DirectionUp]+1)){
                        currentDirection = DirectionRight;
                        newPoint.x++; //Do it manually
                        newPoint.y++;
                        currentRowFilled[DirectionUp]++;
                    }
                    else{
                        break;
                    }
            }
            //APP_LOG(APP_LOG_LEVEL_INFO, "%d, %d", newPoint.x, newPoint.y);
            if(newPoint.x >= AMOUNT_OF_SQUARES_X || newPoint.y >= AMOUNT_OF_SQUARES_Y){
                newSquareToLoad = NULL;
            }
            else{
                newSquareToLoad = squares[newPoint.x][newPoint.y];
            }
            break;
        }
        case AnimationTypeSnakeFromTop:{
            if(square->rawLocation.x == 0 && square->rawLocation.y == 0){
                currentDirection = DirectionRight;
            }

            GPoint currentPoint = square->rawLocation;
            GPoint newPoint = currentPoint;
            switch(currentDirection){
                case DirectionRight:
                    newPoint.x++;
                    if(newPoint.x >= AMOUNT_OF_SQUARES_X){
                        currentDirection = DirectionLeft;
                        newPoint.x--;
                        newPoint.y++;
                    }
                    else{
                        break;
                    }
                case DirectionLeft:
                    newPoint.x--;
                    if(newPoint.x < 0){
                        currentDirection = DirectionRight;
                        newPoint.x++;
                        newPoint.y++;
                    }
                    else{
                        break;
                    }
                default:
                    break;
            }
            //APP_LOG(APP_LOG_LEVEL_INFO, "%d, %d", newPoint.x, newPoint.y);
            if(newPoint.x >= AMOUNT_OF_SQUARES_X || newPoint.y >= AMOUNT_OF_SQUARES_Y){
                newSquareToLoad = NULL;
            }
            else{
                newSquareToLoad = squares[newPoint.x][newPoint.y];
            }
            break;
        }

        default:
            for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
                for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
                    squares[i][i1]->display = true;
                }
            }
            break;
    }

    if(newSquareToLoad != NULL){
        graphics_launch_animation_for_square(newSquareToLoad, ANIMATION_DELAY);
    }
    else{
        animating = false;
    }
}

void graphics_launch_animation_for_square(Square *squareToFirst, int delay){
    app_timer_register(delay, graphics_square_animation, squareToFirst);
}

int squaresShuffled = 0;
void graphics_square_shuffle_animation(void *data){
    Square *square = (Square*)data;

    for(int i = 0; i < (rand() % 4); i++){
        Square *squareToModify = squares[rand() % AMOUNT_OF_SQUARES_X][rand() % AMOUNT_OF_SQUARES_Y];
        squareToModify->colour = graphics_get_colour_for_square(squareToModify);
        squaresShuffled++;
    }

    layer_mark_dirty(graphics_layer);

    Square *newSquareToLoad = NULL;

    #ifdef PBL_ROUND
    if(squaresShuffled > (AMOUNT_OF_SQUARES_X*AMOUNT_OF_SQUARES_Y)/2){
    #else
    if(squaresShuffled > AMOUNT_OF_SQUARES_X*AMOUNT_OF_SQUARES_Y){
    #endif
        squaresShuffled = 0;
    }
    else{
        newSquareToLoad = squares[rand() % AMOUNT_OF_SQUARES_X][rand() % AMOUNT_OF_SQUARES_Y];
    }

    if(newSquareToLoad != NULL){
        graphics_launch_shuffle_animation_for_square(newSquareToLoad, ANIMATION_DELAY);
    }
}

void graphics_launch_shuffle_animation_for_square(Square *squareToFirst, int delay){
    app_timer_register(delay, graphics_square_shuffle_animation, squareToFirst);
}

void graphics_hide_numbers(){
    displayNumbers = false;
    for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
        for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
            if(squares[i][i1]->timeRange != TimeRangeNone){
                squares[i][i1]->colour = graphics_get_colour_for_square(squares[i][i1]);
            }
        }
    }
    layer_mark_dirty(graphics_layer);
}

void graphics_reset_all_squares(){
    for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
        for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
            squares[i][i1]->display = false;
            squares[i][i1]->colour = graphics_get_colour_for_square(squares[i][i1]);
        }
    }
    graphics_launch_animation_for_square(squares[0][0], 1);
}

void graphics_shake_handler(){
    if(animating){
        return;
    }
    switch(graphics_settings.shakeType){
        case ShakeTypeAnimate:
            animating = true;
            graphics_reset_all_squares();
            break;
        case ShakeTypeShuffle:
            graphics_launch_shuffle_animation_for_square(squares[0][0], 1);
            break;
        case ShakeTypeDisplayNumbers:
            displayNumbers = true;
            for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
                for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
                    if(squares[i][i1]->timeRange != TimeRangeNone){
                        squares[i][i1]->colour = graphics_get_colour_for_square(squares[i][i1]);
                    }
                }
            }
            app_timer_register(3000, graphics_hide_numbers, NULL);
            layer_mark_dirty(graphics_layer);
            break;
        default:
            break;
    }
}

void graphics_battery_handler(BatteryChargeState new_charge_state){
    graphics_charge_state = new_charge_state;
    layer_mark_dirty(graphics_layer);
}

void graphics_init(Layer *window_layer){
    graphics_settings = data_framework_get_settings();

    if(graphics_settings.shakeType == ShakeTypeDisplayNumbers){
        displayNumbers = false;
    }

    srand(time(NULL));
    for(int i = 0; i < AMOUNT_OF_SQUARES_X; i++){
        for(int i1 = 0; i1 < AMOUNT_OF_SQUARES_Y; i1++){
            Square *newSquare = square_create(i, i1);
            newSquare->colour = graphics_get_colour_for_square(newSquare);
            newSquare->timeRange = graphics_get_time_range_for_square(newSquare);

            squares[i][i1] = newSquare;
        }
    }

    data_framework_register_settings_callback(graphics_settings_callback, SETTINGS_CALLBACK_GRAPHICS);

    graphics_charge_state = battery_state_service_peek();

    graphics_layer = layer_create(GRect(0, 0, WINDOW_SIZE.w, WINDOW_SIZE.h));
    layer_set_update_proc(graphics_layer, graphics_proc);
    layer_add_child(window_layer, graphics_layer);
    layer_mark_dirty(graphics_layer);

    graphics_launch_animation_for_square(squares[0][0], 50);

    graphics_settings_callback(graphics_settings);
}

void graphics_deinit(){
    layer_destroy(graphics_layer);
}

Square *square_create(int i, int i1){
    Square *new_square = malloc(sizeof(Square));

    int fixedHeightAndWidth = HEIGHT_AND_WIDTH;
    if(graphics_settings.oldStyle){ //Old style
        fixedHeightAndWidth -= 2;
    }

    new_square->location.x = -3+(i*fixedHeightAndWidth) + graphics_settings.oldStyle*i*2;
    new_square->location.y = -1+(i1*fixedHeightAndWidth) + graphics_settings.oldStyle*i1*2;
    if(graphics_settings.oldStyle){ //Old style
        new_square->location.y++;
    }
    new_square->rawLocation.x = i;
    new_square->rawLocation.y = i1;
    new_square->heightAndWidth = fixedHeightAndWidth;

    return new_square;
}

void square_destroy(Square *square){
    free(square);
}