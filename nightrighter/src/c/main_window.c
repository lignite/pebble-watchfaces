#include <pebble.h>
#include "effectlayer/effect_layer.h"
#include "main_window.h"
#include "graphics.h"

Window *main_window;
TextLayer *time_layer;
Settings settings;
Layer *battery_graphics_layer, *other_graphics_layer, *kitt_graphics_layer, *window_layer;
EffectLayer *inverter_layer;
Notification *disconnect_notification, *reconnect_notification;
bool boot = true;

void main_tick_handler(struct tm *t, TimeUnits units){
	static char time_buffer[] = "00:00";
	if(clock_is_24h_style()){
		strftime(time_buffer, sizeof(time_buffer), "%H:%M", t);
	}
	else{
		strftime(time_buffer, sizeof(time_buffer), "%I:%M", t);
	}
	text_layer_set_text(time_layer, time_buffer);

	graphics_set_tm(t);
	if(settings.bootAnimation || settings.alwaysAnimating){
		graphics_set_animating(GRAPHICS_LAYER_KITT, true);
	}
	else{
		graphics_force_update_kitt();
	}
}

void bluetooth_handler(bool connected){
	graphics_set_connected(connected);
	if(boot){
		boot = false;
		return;
	}
	if(connected && settings.btrealert){
		notification_push(reconnect_notification, 5000);
		vibes_double_pulse();
	}
	else if(!connected && settings.btdisalert){
		notification_push(disconnect_notification, 5000);
		vibes_long_pulse();
	}
}

void battery_handler(BatteryChargeState state){
	graphics_set_battery_charge_state(state);
}

void main_window_update_settings(Settings new_settings){
	settings = new_settings;

	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);
  	main_tick_handler(t, MINUTE_UNIT);

  	layer_set_hidden(effect_layer_get_layer(inverter_layer), !settings.invert);
  	vibes_double_pulse();
}

void notification_push_handler(bool pushed, uint8_t id){
	if(pushed){
		layer_remove_from_parent(window_layer);
	}
	else{
		if(disconnect_notification->is_live || reconnect_notification->is_live){
			return;
		}
		layer_add_child(window_get_root_layer(main_window), window_layer);
	}
}

void main_window_load(Window *window){
	Layer *window_root_layer = window_get_root_layer(window);
	GRect frame = layer_get_frame(window_root_layer);
	window_layer = layer_create(frame);
	layer_add_child(window_root_layer, window_layer);

	graphics_init_layers(window_layer);

	settings = data_framework_get_settings();

	#ifdef PBL_ROUND
	GRect timeFrame = GRect(0, 74, frame.size.w, 30);
	#else
	GRect timeFrame = GRect(0, 66, frame.size.w, 30);
	#endif

	time_layer = text_layer_init(timeFrame, GTextAlignmentCenter, fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_IMPACT_28)));
	text_layer_set_text(time_layer, "Time");
	layer_add_child(window_layer, text_layer_get_layer(time_layer));

	GBitmap *bluetoothIcon = gbitmap_create_with_resource(RESOURCE_ID_LIGNITE_IMAGE_BLUETOOTH_ICON);

    disconnect_notification = notification_create(window);
    notification_set_icon(disconnect_notification, bluetoothIcon);
    notification_set_accent_colour(disconnect_notification, PBL_IF_COLOR_ELSE(GColorRed, GColorWhite));
    notification_set_contents(disconnect_notification, "Oh boy...", "I lost connection to\nyour phone, sorry!");

    reconnect_notification = notification_create(window);
    notification_set_icon(reconnect_notification, bluetoothIcon);
    notification_set_accent_colour(reconnect_notification, PBL_IF_COLOR_ELSE(GColorBlue, GColorWhite));
    notification_set_contents(reconnect_notification, "Woohoo!", "You are now\nconnected to your\nphone again!");

    notification_register_push_handler(notification_push_handler);

	inverter_layer = effect_layer_create(GRect(0, 0, frame.size.w, frame.size.h));
	effect_layer_add_effect(inverter_layer, effect_invert, NULL);
	layer_set_hidden(effect_layer_get_layer(inverter_layer), !settings.invert);
	layer_add_child(window_layer, effect_layer_get_layer(inverter_layer));

	data_framework_register_settings_callback(main_window_update_settings, SETTINGS_CALLBACK_MAIN_WINDOW);

	BatteryChargeState s = battery_state_service_peek();
	battery_handler(s);

	bool c = bluetooth_connection_service_peek();
	bluetooth_handler(c);

	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);
  	main_tick_handler(t, MINUTE_UNIT);
}

void main_window_unload(Window *window){

}

void main_window_init(){
	main_window = window_create();
	window_set_background_color(main_window, GColorBlack);
	window_set_window_handlers(main_window, (WindowHandlers){
		.load = main_window_load,
		.unload = main_window_unload
	});
	tick_timer_service_subscribe(MINUTE_UNIT, main_tick_handler);
	bluetooth_connection_service_subscribe(bluetooth_handler);
	battery_state_service_subscribe(battery_handler);
}

Window *main_window_get_window(){
	return main_window;
}

void main_window_deinit(){
	window_destroy(main_window);
}
