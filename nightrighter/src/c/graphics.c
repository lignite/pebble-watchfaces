#include <pebble.h>
#include "lignite.h"
#include "graphics.h"

int day, date, month;
bool battery_to_draw[10];
bool graphics_connected = false;
bool animating[GRAPHICS_AMOUNT_OF_LAYERS];
BatteryChargeState graphics_charge_state;
GColor background_colour, colour1, colour2, colour3;
Settings graphics_settings;

BitmapLayer *bluetooth_layer, *background_layer;
Layer *graphics_layers[GRAPHICS_AMOUNT_OF_LAYERS];
Layer *cover_layers[2];
AppTimer *animation_timers[GRAPHICS_AMOUNT_OF_LAYERS];
static char *wdays[7] = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
static char *percent_buffers[10] = { "100", "100", "100", "100", "100", "100", "100", "100", "100", "100" };

void graphics_battery_graphics_proc(Layer *layer, GContext *ctx){
    #ifdef PBL_COLOR
        graphics_context_set_antialiased(ctx, graphics_settings.antialiasing);
    #endif

    graphics_context_set_fill_color(ctx, colour3);
    for(int i = 0; i < 10; i++){
        if(i < 4){ graphics_context_set_fill_color(ctx, colour1); } else if(i > 3 && i < 7){ graphics_context_set_fill_color(ctx, colour2); } else{ graphics_context_set_fill_color(ctx, colour3); }
        if(((graphics_charge_state.charge_percent/10) >= 10-i) && battery_to_draw[9-i]){
            graphics_fill_rect(ctx, GRect(114, 10+(15*i), 20, 12), 2, GCornersAll);
            snprintf(percent_buffers[i], sizeof(percent_buffers[i]), "%d", 100-(i*10));
            graphics_context_set_text_color(ctx, background_colour);
            graphics_draw_text(ctx, percent_buffers[i], fonts_get_system_font(FONT_KEY_GOTHIC_14), GRect(114, 6+(15*i), 20, 11), GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);
        }
    }
}

void graphics_other_graphics_proc(Layer *layer, GContext *ctx){
    #ifdef PBL_COLOR
        graphics_context_set_antialiased(ctx, graphics_settings.antialiasing);
    #endif
    graphics_context_set_fill_color(ctx, colour3);
    for(int i = 0; i < 4; i++){
        if(i < 2){ graphics_context_set_fill_color(ctx, colour1); } else { graphics_context_set_fill_color(ctx, colour3); }
        graphics_fill_rect(ctx, GRect(6, 10+(41*i), 24, 20), 8, GCornersAll);
        switch(i){
            case 0:;
                static char date_buffer_wday[] = "Fr";
                snprintf(date_buffer_wday, sizeof(date_buffer_wday), "%s", wdays[day]);
                graphics_context_set_text_color(ctx, background_colour);
                graphics_draw_text(ctx, date_buffer_wday, fonts_get_system_font(FONT_KEY_GOTHIC_14), GRect(6, 10, 24, 14), GTextOverflowModeFill, GTextAlignmentCenter, NULL);
                break;
            case 1:;
                static char date_buffer_month[] = "30";
                snprintf(date_buffer_month, sizeof(date_buffer_month), "%d", month);
                graphics_context_set_text_color(ctx, background_colour);
                graphics_draw_text(ctx, date_buffer_month, fonts_get_system_font(FONT_KEY_GOTHIC_14_BOLD), GRect(6, 52, 24, 14), GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);
                break;
            case 2:;
                static char date_buffer_day[] = "42";
                snprintf(date_buffer_day, sizeof(date_buffer_day), "%d", date);
                graphics_context_set_text_color(ctx, background_colour);
                graphics_draw_text(ctx, date_buffer_day, fonts_get_system_font(FONT_KEY_GOTHIC_14_BOLD), GRect(6, 93, 24, 14), GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);
                break;
            case 3:;
                break;
        }
    }
    layer_set_hidden(bitmap_layer_get_layer(bluetooth_layer), !graphics_connected);
}

void graphics_cover_layer_proc_0(Layer *layer, GContext *ctx){
    graphics_context_set_fill_color(ctx, background_colour);
    graphics_fill_rect(ctx, GRect(42, 8, 16, 58), 0, GCornerNone);
    graphics_fill_rect(ctx, GRect(60, 6, 22, 50), 0, GCornerNone);
    graphics_fill_rect(ctx, GRect(84, 28, 18, 38), 0, GCornerNone);
}

void graphics_cover_layer_proc_1(Layer *layer, GContext *ctx){
    graphics_context_set_fill_color(ctx, background_colour);
    graphics_fill_rect(ctx, GRect(40, 100, 18, 48), 0, GCornerNone);
    graphics_fill_rect(ctx, GRect(62, 110, 22, 50), 0, GCornerNone);
    graphics_fill_rect(ctx, GRect(85, 100, 18, 48), 0, GCornerNone);
}

void graphics_battery_callback(){
    int amount_open = 0;
    for(int i = 0; i < 10; i++){
        if(battery_to_draw[i]){
            amount_open++;
        }
    }
    if((amount_open+1) < 10){
        battery_to_draw[amount_open+1] = true;
    }
    else if((amount_open+1) == 9){
        battery_to_draw[amount_open] = true;
    }
    else{
        for(int i = 0; i < 10; i++){
            battery_to_draw[i] = false;
        }
    }

    layer_mark_dirty(graphics_layers[GRAPHICS_LAYER_BATTERY]);
    if(animating[GRAPHICS_LAYER_BATTERY]){
        animation_timers[GRAPHICS_LAYER_BATTERY] = app_timer_register(125, graphics_battery_callback, NULL);
    }
    else{
        for(int i = 0; i < 10; i++){
            battery_to_draw[i] = true;
        }
    }
}

void graphics_kitt_callback(){
    animating[GRAPHICS_LAYER_KITT] = false;
    if(graphics_settings.alwaysAnimating){
        graphics_set_animating(GRAPHICS_LAYER_KITT, true);
    }
}

void graphics_set_animating(GraphicsLayer layer, bool anim){
    switch(layer){
        case GRAPHICS_LAYER_BATTERY:
            if(anim && !animating[layer]){
                animation_timers[GRAPHICS_LAYER_BATTERY] = app_timer_register(100, graphics_battery_callback, NULL);
            }
            break;
        case GRAPHICS_LAYER_KITT:;
            if(animating[GRAPHICS_LAYER_KITT]){
                return;
            }
            int randPick = rand() % 4;
            int memes = 300;
            animation_timers[GRAPHICS_LAYER_KITT] = app_timer_register(2500, graphics_kitt_callback, NULL);
            switch(randPick){
                case 0:;
                    GRect frame = layer_get_frame(cover_layers[0]);
                    animate_layer(cover_layers[0], &frame, &GRect(0, -15, 144, 168), 300, 0+memes);
                    animate_layer(cover_layers[0], &GRect(0, -15, 144, 168), &GRect(0, -30, 144, 168), 300, 300+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -20, 144, 168), 300, 600+memes);
                    animate_layer(cover_layers[0], &GRect(0, -20, 144, 168), &GRect(0, -40, 144, 168), 250, 900+memes);
                    animate_layer(cover_layers[0], &GRect(0, -40, 144, 168), &GRect(0, -30, 144, 168), 300, 1150+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -70, 144, 168), 300, 1550+memes);

                    GRect frame1 = layer_get_frame(cover_layers[1]);
                    animate_layer(cover_layers[1], &frame1, &GRect(0, 15, 144, 168), 300, 0+memes);
                    animate_layer(cover_layers[1], &GRect(0, 15, 144, 168), &GRect(0, 30, 144, 168), 300, 300+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 20, 144, 168), 300, 600+memes);
                    animate_layer(cover_layers[1], &GRect(0, 20, 144, 168), &GRect(0, 40, 144, 168), 250, 900+memes);
                    animate_layer(cover_layers[1], &GRect(0, 40, 144, 168), &GRect(0, 30, 144, 168), 300, 1150+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 70, 144, 168), 300, 1550+memes);
                    break;
                case 1:;
                    GRect frame01 = layer_get_frame(cover_layers[0]);
                    animate_layer(cover_layers[0], &frame01, &GRect(0, -15, 144, 168), 300, 0+memes);
                    animate_layer(cover_layers[0], &GRect(0, -15, 144, 168), &GRect(0, -30, 144, 168), 300, 300+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -20, 144, 168), 300, 600+memes);
                    animate_layer(cover_layers[0], &GRect(0, -20, 144, 168), &GRect(0, 0, 144, 168), 250, 900+memes);
                    animate_layer(cover_layers[0], &GRect(0, 0, 144, 168), &GRect(0, -30, 144, 168), 300, 1150+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -70, 144, 168), 300, 1450+memes);

                    GRect frame11 = layer_get_frame(cover_layers[1]);
                    animate_layer(cover_layers[1], &frame11, &GRect(0, 15, 144, 168), 300, 0+memes);
                    animate_layer(cover_layers[1], &GRect(0, 15, 144, 168), &GRect(0, 30, 144, 168), 300, 300+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 20, 144, 168), 300, 600+memes);
                    animate_layer(cover_layers[1], &GRect(0, 20, 144, 168), &GRect(0, 0, 144, 168), 250, 900+memes);
                    animate_layer(cover_layers[1], &GRect(0, 0, 144, 168), &GRect(0, 30, 144, 168), 300, 1150+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 70, 144, 168), 300, 1450+memes);
                    break;
                case 2:;
                    GRect frame02 = layer_get_frame(cover_layers[0]);
                    animate_layer(cover_layers[0], &frame02, &GRect(0, -30, 144, 168), 300, 0+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -20, 144, 168), 300, 300+memes);
                    animate_layer(cover_layers[0], &GRect(0, -20, 144, 168), &GRect(0, -50, 144, 168), 250, 600+memes);
                    animate_layer(cover_layers[0], &GRect(0, -50, 144, 168), &GRect(0, -10, 144, 168), 300, 850+memes);
                    animate_layer(cover_layers[0], &GRect(0, -10, 144, 168), &GRect(0, -70, 144, 168), 300, 1150+memes);

                    GRect frame12 = layer_get_frame(cover_layers[1]);
                    animate_layer(cover_layers[1], &frame12, &GRect(0, 30, 144, 168), 300, 0+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 20, 144, 168), 300, 300+memes);
                    animate_layer(cover_layers[1], &GRect(0, 20, 144, 168), &GRect(0, 50, 144, 168), 250, 600+memes);
                    animate_layer(cover_layers[1], &GRect(0, 50, 144, 168), &GRect(0, 10, 144, 168), 300, 850+memes);
                    animate_layer(cover_layers[1], &GRect(0, 10, 144, 168), &GRect(0, 70, 144, 168), 300, 1150+memes);
                    break;
                case 3:;
                    GRect frame03 = layer_get_frame(cover_layers[0]);
                    animate_layer(cover_layers[0], &frame03, &GRect(0, -30, 144, 168), 250, 0+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -20, 144, 168), 250, 250+memes);
                    animate_layer(cover_layers[0], &GRect(0, -20, 144, 168), &GRect(0, -50, 144, 168), 250, 500+memes);
                    animate_layer(cover_layers[0], &GRect(0, -50, 144, 168), &GRect(0, -10, 144, 168), 250, 750+memes);
                    animate_layer(cover_layers[0], &GRect(0, -10, 144, 168), &GRect(0, -70, 144, 168), 250, 1000+memes);
                    animate_layer(cover_layers[0], &GRect(0, -70, 144, 168), &GRect(0, -0, 144, 168), 250, 1250+memes);
                    animate_layer(cover_layers[0], &GRect(0, 0, 144, 168), &GRect(0, -40, 144, 168), 250, 1500+memes);
                    animate_layer(cover_layers[0], &GRect(0, -40, 144, 168), &GRect(0, -50, 144, 168), 250, 1750+memes);
                    animate_layer(cover_layers[0], &GRect(0, -50, 144, 168), &GRect(0, -30, 144, 168), 250, 2000+memes);
                    animate_layer(cover_layers[0], &GRect(0, -30, 144, 168), &GRect(0, -70, 144, 168), 250, 2250+memes);

                    GRect frame13 = layer_get_frame(cover_layers[1]);
                    animate_layer(cover_layers[1], &frame13, &GRect(0, 30, 144, 168), 250, 0+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 20, 144, 168), 250, 250+memes);
                    animate_layer(cover_layers[1], &GRect(0, 20, 144, 168), &GRect(0, 50, 144, 168), 250, 500+memes);
                    animate_layer(cover_layers[1], &GRect(0, 50, 144, 168), &GRect(0, 10, 144, 168), 250, 750+memes);
                    animate_layer(cover_layers[1], &GRect(0, 10, 144, 168), &GRect(0, 70, 144, 168), 250, 1000+memes);
                    animate_layer(cover_layers[1], &GRect(0, 70, 144, 168), &GRect(0, 0, 144, 168), 250, 1250+memes);
                    animate_layer(cover_layers[1], &GRect(0, 0, 144, 168), &GRect(0, 40, 144, 168), 250, 1500+memes);
                    animate_layer(cover_layers[1], &GRect(0, 40, 144, 168), &GRect(0, 50, 144, 168), 250, 1750+memes);
                    animate_layer(cover_layers[1], &GRect(0, 50, 144, 168), &GRect(0, 30, 144, 168), 250, 2000+memes);
                    animate_layer(cover_layers[1], &GRect(0, 30, 144, 168), &GRect(0, 70, 144, 168), 250, 2250+memes);
                    break;
            }
            break;
        default:;
    }
    animating[layer] = anim;
}

void graphics_set_tm(struct tm *t){
    day = t->tm_wday;
    date = t->tm_mday;
    month = t->tm_mon+1;
    layer_mark_dirty(graphics_layers[GRAPHICS_LAYER_OTHER]);
}

void graphics_set_battery_charge_state(BatteryChargeState new_state){
    graphics_charge_state = new_state;
    layer_mark_dirty(graphics_layers[GRAPHICS_LAYER_BATTERY]);
    graphics_set_animating(GRAPHICS_LAYER_BATTERY, new_state.is_charging);
}

void graphics_set_connected(bool connected){
    graphics_connected = connected;
}

void graphics_update_settings(Settings new_settings){
    graphics_settings = new_settings;
    for(int i = 0; i < GRAPHICS_AMOUNT_OF_LAYERS-1; i++){
        layer_mark_dirty(graphics_layers[i]);
    }
}

void graphics_force_update_kitt(){
    layer_set_frame(cover_layers[1], GRect(0, 70, 144, 168));
    layer_set_frame(cover_layers[0], GRect(0, -70, 144, 168));
}

void graphics_init_layers(Layer *window_layer){
    srand(time(NULL));

    data_framework_register_settings_callback(graphics_update_settings, SETTINGS_CALLBACK_GRAPHICS);

    #ifdef PBL_COLOR
        background_colour = GColorBlack;
        colour1 = GColorYellow;
        colour2 = GColorChromeYellow;
        colour3 = GColorRed;
    #else
        background_colour = GColorBlack;
        colour1 = GColorWhite;
        colour2 = GColorWhite;
        colour3 = GColorWhite;
    #endif

    for(int i = 0; i < 10; i++){
        battery_to_draw[i] = true;
    }

    background_layer = bitmap_layer_create(GRect(0, 0, 144, 168));
    #ifdef PBL_COLOR
    bitmap_layer_set_bitmap(background_layer, gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND_COLOUR));
    bitmap_layer_set_compositing_mode(background_layer, GCompOpSet);
    #else
    bitmap_layer_set_bitmap(background_layer, gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND_BLACK_AND_WHITE));
    #endif
    layer_add_child(window_layer, bitmap_layer_get_layer(background_layer));

    graphics_layers[GRAPHICS_LAYER_BATTERY] = layer_create(GRect(0, 0, 144, 168));
    layer_set_update_proc(graphics_layers[GRAPHICS_LAYER_BATTERY], graphics_battery_graphics_proc);
    layer_add_child(window_layer, graphics_layers[GRAPHICS_LAYER_BATTERY]);

    graphics_layers[GRAPHICS_LAYER_OTHER] = layer_create(GRect(0, 0, 144, 168));
    layer_set_update_proc(graphics_layers[GRAPHICS_LAYER_OTHER], graphics_other_graphics_proc);
    layer_add_child(window_layer, graphics_layers[GRAPHICS_LAYER_OTHER]);

    cover_layers[0] = layer_create(GRect(0, 0, 144, 168));
    layer_set_update_proc(cover_layers[0], graphics_cover_layer_proc_0);
    layer_add_child(window_layer, cover_layers[0]);

    cover_layers[1] = layer_create(GRect(0, 0, 144, 168));
    layer_set_update_proc(cover_layers[1], graphics_cover_layer_proc_1);
    layer_add_child(window_layer, cover_layers[1]);

    bluetooth_layer = bitmap_layer_create(GRect(8, 134, 20, 20));
    bitmap_layer_set_bitmap(bluetooth_layer, gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BLUETOOTH_ICON_BLACK));
    bitmap_layer_set_compositing_mode(bluetooth_layer, GCompOpClear);
    layer_add_child(window_layer, bitmap_layer_get_layer(bluetooth_layer));
}

Layer *graphics_get_layer(GraphicsLayer layer){
    return graphics_layers[layer];
}

void graphics_deinit_layers(){
    for(int i = 0; i < GRAPHICS_AMOUNT_OF_LAYERS-1; i++){
        layer_destroy(graphics_layers[i]);
    }
}
